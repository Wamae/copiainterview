package com.company;

public class PaymentAllocation {
    private String receipt;
    private String mpesaRef;
    private float amount;

    public PaymentAllocation(String receipt, String mpesaRef, float amount) {
        this.receipt = receipt;
        this.mpesaRef = mpesaRef;
        this.amount = amount;
    }

    public String getReceipt() {
        return receipt;
    }

    public void setReceipt(String receipt) {
        this.receipt = receipt;
    }

    public String getMpesaRef() {
        return mpesaRef;
    }

    public void setMpesaRef(String mpesaRef) {
        this.mpesaRef = mpesaRef;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return this.receipt+" "+this.mpesaRef+" "+this.amount;
    }
}
