package com.company;

public class Payment {

    private String mpesaRef;
    private float amount;

    public Payment(String mpesaRef, float amount) {
        this.mpesaRef = mpesaRef;
        this.amount = amount;
    }

    public String getMpesaRef() {
        return mpesaRef;
    }

    public void setMpesaRef(String mpesaRef) {
        this.mpesaRef = mpesaRef;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }
}
