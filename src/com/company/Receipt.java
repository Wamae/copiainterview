package com.company;

public class Receipt {
    private String receipt;
    private float amount;

    public Receipt(String receipt, float amount) {
        this.receipt = receipt;
        this.amount = amount;
    }

    public String getReceipt() {
        return receipt;
    }

    public void setReceipt(String receipt) {
        this.receipt = receipt;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }
}
