package com.company;


import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Receipt> receipts;
        List<Payment> payments;

        receipts = new ArrayList<>();
        receipts.add(new Receipt("R001", 100));
        receipts.add(new Receipt("R002", 400));
        receipts.add(new Receipt("R003", 350));

        payments = new ArrayList<>();
        payments.add(new Payment("MG001", 100));
        payments.add(new Payment("MG002", 200));
        payments.add(new Payment("MG003", 300));
        payments.add(new Payment("MG004", 250));


        List<PaymentAllocation> allocations = new ArrayList<>();

        Receipt remRcpt = null;


        //loop through receipts
        for (int r = 0; r < receipts.size(); r++) {

        //loop through payments
        for (int p = 0; p < payments.size(); p++) {


                if (payments.get(p) != null) {

                    //if amount is >=, allocate and deduct
                    Payment payment = payments.get(p);
                    Receipt receipt = receipts.get(r);

                    float paymentAmount = payment.getAmount();
                    float receiptAmount = receipt.getAmount();

                    String receiptRef = receipt.getReceipt();
                    String mpesaRef = payment.getMpesaRef();

                    float remainder = receiptAmount - paymentAmount;

                    //System.out.println("Allocation: " + "r:" + r + " p:" + p);

                    if (remainder == 0.0) {
                        PaymentAllocation allocation = new PaymentAllocation(receiptRef, mpesaRef, receiptAmount);
                        allocations.add(allocation);
                        System.out.println(allocation.toString());

                        payments.set(p, null);
                        break;
                    } else if (remainder > 0.0) {
                        remRcpt = new Receipt(receiptRef, remainder);
                        //System.out.println("1 Remainder: " + remainder);

                        PaymentAllocation allocation = new PaymentAllocation(receiptRef, mpesaRef, remainder);
                        allocations.add(allocation);
                        System.out.println(allocation.toString());

                        receipts.set(r, remRcpt);
                        continue;

                    } else {

                        Payment remPayment = new Payment(mpesaRef, Math.abs(remainder));
                        //System.out.println("2 Remainder: " + remainder);

                        PaymentAllocation allocation = new PaymentAllocation(receiptRef, mpesaRef, receiptAmount);
                        allocations.add(allocation);
                        System.out.println(allocation.toString());

                        payments.set(p, remPayment);

                        break;

                    }
                }
            }
        }

    }
}
